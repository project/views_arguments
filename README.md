Views Arguments adds several useful Views tricks.

One of them (Access) requires a Views patch, because standard Views doesn't allow
outside modules to add custom options to Views handlers.

Patch: https://www.drupal.org/node/2610418#comment-10538690
